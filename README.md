# Workshop Web App API

## Table of Contents
* [Coding Standars](#markdown-coding-standards)
* [Requirements](#markdown-header-requirements)
* [Pre-Setup](#markdown-header-pre-setup)
* [Setup](#markdown-header-setup)
* [Config](#markdown-header-config)
* [API-Doc](#markdown-header-api-doc)
* [Testing](#markdown-header-testing)
    * [Automated Unit Tests](#markdown-header-automated-unit-tests)
    * [End-to-End Test Helpers](#markdown-header-end-to-end-test-helpers)
* [Deployment](#markdown-header-deployment)

## Coding Standards
We use GIT to control project's versions, and use [Git-Flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) like our base four the work flow. Here are some important statements that you have to consider:
* The branches are named 'feature' or 'hotfix' depending on the type of task.
* The commit needs a simple and clear description.
* Example: 'feature/client-authentication'

You can access to the following [link](https://chris.beams.io/posts/git-commit/) for help.

## Requirements
* Ruby v2.5.1
* node v8/v10 with npm (for [API-Doc](#markdown-header-api-doc) generation)

## Pre-Setup
1. Install Ruby
To install ruby there are several options, here are some of them:
    * [chruby](https://github.com/postmodern/chruby#install) (recommended)
    * [rvm](https://rvm.io/rvm/install) (recommended)
    * [rbenv](https://github.com/rbenv/rbenv#readme)
    * [uru](https://bitbucket.org/jonforums/uru/src/master/)

    However, no matter the way you manage Ruby, don't forget to use _**version 2.3.7+**_

3. Install Bundler

    Previous to initiate the setup go to the workspace and install the bundler
    ```sh
    gem install bundler -v 1.16.6
    ```

4. Install node

    For installing node we highly recommend the use of [nvm](https://github.com/creationix/nvm#node-version-manager---), after [installing nvm](https://github.com/creationix/nvm#installation) run

    ```sh
    nvm install --lts
    ```

    this will download the last LTS version of node with npm

## Setup
1. Clone the repository and enter the folder
    ```sh
       git clone https://{user}@bitbucket.org/workshoptech/api.git
    cd workshop-api
    ```

2. Go to development workspace
    ```sh
    git fetch --all && git checkout develop
    ```

3. Install gems (don't forget to install bundler!)
    ```sh
    bundle install
    ```

4. Setup the environment variables
    1. **Create CI env file**

        - `sh cp ci/application.ci.example.yml ci/application.ci.yml`

    2. **Create config env file**

        - `sh cp config/application.example.yml config/application.yml`

5. Initialize the database
    ```sh
    rails db:create &&
    rails db:migrate
    ```

6. Start the server
    - **Option 1**

        - `sh rails server`

    - **Option 2**

        - `bundle exec rails server`

## Config
TODO

## API-Doc
To watch the API documentation, first you need to install the node packages:

1. `sh npm install`  
2. `sh npm run serve-documentation`
3. Then go to http://localhost:5000

## Postman
We have a Postman Workspace (for an invitation please ask administrator) that has an updated
suite of requests to facilitate integrations.

1. Start the server with `rails s`
2. Open [localhost](http://localhost:3000/manual_test/firebase_auth_tokens) on your browser
3. Copy the value that appears as idToken
4. Open Postman and go to the team workspace
5. Create a new environment with the a *url* variable with initial value as `http://localhost:3000` and a *token* variable with a blank value at the moment.
6. Go to Workshop API folder, and select the `api/v1/session/create` request, in the body tab replace the authentication_token with the previously generated idToken.
7. Send the request and copy the response `accessToken`
8. Set the value of the previously *token* environment variable that was left blank
9. Start testing your API

## Testing
### Automated Unit Tests
To run testing, run:

- **Option 1**    
    - `sh rspec`

- **Option 2**
    - `bundle exec rspec`

### End-to-End Test Helpers
To run end-to-end tests (eg. Postman) you will need to get a Firebase user id token, that is because all endpoints use `Bearer Token Authentication` and in order to get that bearer you will need to log in. For more information about the login check the [API-Doc](#API-Doc)

To get this Firebase token you need to start your server and go to http://localhost:3000/manual_test/firebase_auth_tokens
and follow instructions to get your token.


## Deployment
### Semantic Versioning
We use [Semantic Versioning](https://semver.org/) for keeping our project versions simple, the project
version file is located in `config/initializers/version.rb`. We also use the [app_version_tasks](https://github.com/darrenleeweber/app_version_tasks) gem as tool for easy version bumping and handling tag releases, this are the options:
```
version:bump:major - bump and commit the major version (x in x.0.0)
                   - resets the minor and patch versions to zero
                   - does not push the local workspace to 'origin'
version:bump:minor - bump and commit the minor version (y in x.y.0)
                   - resets the patch version to zero
                   - does not push the local workspace to 'origin'
version:bump:patch - bump and commit the patch version (z in x.y.z)
                   - does not push the local workspace to 'origin'
version:current    - the current version
version:release    - tag and push the current version
                   - pushes to 'origin/{current_branch}'
```

### Heroku
We use [Heroku](https://www.heroku.com/) as our server infrastructure.
```
heroku pg:reset            - resets the database
heroku run rake db:migrate - run the database migrations
heroku run rake db:seed    - run the database seeds
heroku restart             - restart the heroku dyno
```
