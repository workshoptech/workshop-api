class Api::ApprovalRequestsController < ApplicationController
  before_action :set_approval_request, only: [:show, :update, :destroy]
  before_action :set_user, only: [:create, :update]
  before_action :set_workshop
  
  def index
    @approval_requests = @workshop.approval_requests
  end

  def show
  end

  def create
    @approval_request = ApprovalRequest.find_or_create_by(workshop_id: @workshop.id, user_id: @user.id, status: :pending)
    respond_to do |format|
      if @approval_request.save
        format.json { render :show, status: :created }
      else
        format.json { render json: @approval_request.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @approval_request.update(approval_request_params)
        format.json { render :show, status: :ok }
      else
        format.json { render json: @approval_request.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @approval_request.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
    def set_approval_request
      @approval_request = ApprovalRequest.find(params[:id])
    end

    def set_workshop
      @workshop = Workshop.find(params[:workshop_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def approval_request_params
      params.require(:approval_request).permit(:comment, :status, :workshop_id, :user_id)
    end
end
