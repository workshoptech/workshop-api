class API::InscriptionsController < ApplicationController
  before_action :set_inscription, only: [:show, :edit, :update, :destroy]

  # GET /inscriptions
  def index
    @inscriptions = Inscription.all
  end

  # GET /inscriptions/1
  def show
  end

  # GET /inscriptions/new
  def new
    @inscription = Inscription.new
  end

  # GET /inscriptions/1/edit
  def edit
  end

  # POST /inscriptions
  def create
    @inscription = Inscription.new(inscription_params)

    respond_to do |format|
      if @inscription.save
        format.json { render :show, status: :created, location: @inscription }
      else
        format.json { render json: @inscription.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /inscriptions/1
  def update
    respond_to do |format|
      if @inscription.update(inscription_params)
        format.json { render :show, status: :ok, location: @inscription }
      else
        format.json { render json: @inscription.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /inscriptions/1
  def destroy
    @inscription.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_inscription
      @inscription = Inscription.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def inscription_params
      params.fetch(:inscription, {})
    end
end
