class Api::MessagesController < ApplicationController
  before_action :set_message, only: [:show, :update, :destroy]
  before_action :set_user, only: [:create]
  include NotificationsHelper

  # GET /messages
  # GET /messages.json
  def index
    @messages = Message.all
  end

  # GET /messages/1
  # GET /messages/1.json
  def show
  end

  # POST /messages
  # POST /messages.json
  def create
    @message = Message.create(
      author: @user,
      body: params[:message][:body],
      destinatary_type: params[:message][:destinatary_type]
    )

    case params[:message][:destinatary_type]
    when 'general'
      User.all.each { |destinatary| create_notification(destinatary, @message) }
    when 'workshop'
      workshop = Workshop.find(params[:message][:workshop_id])
      workshop.subscribers.each { |destinatary| create_workshop_notification(destinatary, @message, workshop) }
    when 'user'
      users = User.find(message_params[:users_ids])
      users.each { |destinatary| create_notification(destinatary, @message) }
    end

    respond_to do |format|
      format.json { head :ok }
    end
  end

  # PATCH/PUT /messages/1
  # PATCH/PUT /messages/1.json
  def update
    if @message.update(message_params)
      render :show, status: :ok, location: @message
    else
      render json: @message.errors, status: :unprocessable_entity
    end
  end

  # DELETE /messages/1
  # DELETE /messages/1.json
  def destroy
    @message.destroy
  end

  # GET /messages/destinataries
  def destinataries
    @workshops = Workshop.where(is_approved: true)
    @users = User.all
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_message
      @message = Message.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def message_params
      params.require(:message).permit(
        :body,
        :destinatary_type,
        :workshop_id,
        :users_ids => []
      )
    end
end
