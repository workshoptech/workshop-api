class Api::NotificationsController < ApplicationController
  before_action :set_notification, only: [:show, :edit, :update, :destroy]
  before_action :set_user, only: [:by_user, :brief]

  # GET /notifications
  def index
    @notifications = Notification.all
  end

  # GET /notifications/1
  def show
  end

  def by_user
    @notifications = @user.notifications
    render 'index'
  end

  def brief
    @notifications = @user.notifications.last(5)
    render 'index'
  end

  # DELETE /notifications/1
  def destroy
    @notification.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_notification
      @notification = Notification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def notification_params
      params.fetch(:notification, {})
    end
end
