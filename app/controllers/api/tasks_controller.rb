class Api::TasksController < ApplicationController
  before_action :authenticate_api_user!
  before_action :set_task, only: [:show, :edit, :update, :destroy]
  before_action :refresh_trello_tasks
  include TasksHelper

  # GET /tasks
  def index
    admin_users = User.where(role: [:admin, :super_admin])
    @tasks = Array.new
    for admin in admin_users do
      admin_info = [0,0,0,0,0,0,0]
      admin_tasks = admin.tasks
      numberOfTasksOnReview = admin_tasks.where(status: :on_review).count
      total_worked_time = admin_tasks.sum(:worked_time)
      admin_info[0] = admin.id
      admin_info[1] = admin.name
      admin_info[2] = admin.lastname
      admin_info[3] = numberOfTasksOnReview
      admin_info[4] = admin.photo_url
      admin_info[5] = total_worked_time
      admin_info[6] = admin.company
      @tasks.push(admin_info)
    end
  end

  # GET /tasks/1
  def show

  end

  # GET /tasks/new
  def new
    @task = Task.new
  end

  # GET /tasks/1/edit
  def edit
  end

  # POST /tasks
  def create
    @task = Task.new(task_params)

    respond_to do |format|
      if @task.save
        format.json { render :show, status: :created, location: @task }
      else
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tasks/1
  def update
    respond_to do |format|
      if @task.update(task_params)
        format.json { render :show, status: :ok, location: @task }
      else
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tasks/1
  def destroy
    @task.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task
      @task = Task.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def task_params
      params.fetch(:task, {})
    end
end
