class API::TestimoniesController < ApplicationController
  before_action :set_testimony, only: [:show, :edit, :update, :destroy]

  # GET /testimonies
  def index
    @testimonies = Testimony.all
  end

  # GET /testimonies/1
  def show
  end

  # GET /testimonies/new
  def new
    @testimony = Testimony.new
  end

  # GET /testimonies/1/edit
  def edit
  end

  # POST /testimonies
  def create
    @testimony = Testimony.new(testimony_params)

    respond_to do |format|
      if @testimony.save
        format.json { render :show, status: :created, location: @testimony }
      else
        format.json { render json: @testimony.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /testimonies/1
  def update
    respond_to do |format|
      if @testimony.update(testimony_params)
        format.json { render :show, status: :ok, location: @testimony }
      else
        format.json { render json: @testimony.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /testimonies/1
  def destroy
    @testimony.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_testimony
      @testimony = Testimony.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def testimony_params
      params.fetch(:testimony, {})
    end
end
