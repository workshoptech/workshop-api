class Api::UsersController < ApplicationController
  before_action :authenticate_api_user!
  before_action :set_user, only: [:show, :update, :destroy]

  # GET /users
  def index
    @users = User.all
  end

  # GET /users/1
  def show
  end

  # PATCH/PUT /users/1
  def update
    account_can_edit = (current_api_user.super_admin? or current_api_user.admin?)
    respond_to do |format|
      if account_can_edit and @user.update(user_params)
        format.json { render :show, status: :ok }
      else
        format.json { render json: @user.errors, status: :forbidden }
      end
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:role, :trello_id, :trello_username, :trello_token, :photo_url, :company)
    end
end
