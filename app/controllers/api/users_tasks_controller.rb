class Api::UsersTasksController < ApplicationController
    before_action :set_user, only: [:index, :show]


    def index
        @user_name = @user.name
        @user_lastname = @user.lastname
        @tasks = @user.tasks.where(status: [:on_review, :done])
        @total_worked_time = @tasks.sum(:worked_time)
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:user_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.fetch(:user, {})
    end
end