class Api::WorkshopsController < ApplicationController
  before_action :set_workshop, only: [
    :show,
    :edit,
    :update,
    :destroy,
    :subscribe,
    :unsubscribe
  ]
  before_action :set_user, only: [
    :create,
    :signed,
    :subscribe,
    :unsubscribe,
    :speaker_workshops,
    :show,
    :brief
  ]

  # GET /workshops
  def index
    @workshops = Workshop.where(is_approved: true)
  end

  # GET /workshops/1
  def show
  end

  # GET /workshops/new
  def new
    @workshop = Workshop.new
  end

  # GET /workshops/1/edit
  def edit
  end

  # POST /workshops
  def create
    @workshop = Workshop.new(workshop_params)
    @workshop.speaker = @user
    respond_to do |format|
      if @workshop.save
        approval_request = ApprovalRequest.create(workshop: @workshop)
        approval_request.speaker = @user
        approval_request.save!
        format.json { render :show, status: :created }
      else
        format.json { render json: @workshop.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /workshops/1
  def update
    if workshop_params.has_key?(:speaker_id)
      speaker = User.find_by(email: workshop_params[:speaker_id])
      if speaker.nil?
        render json: @workshop.errors, status: 404
        return
      end
      @workshop.speaker = speaker
    end
    
    respond_to do |format|
      if @workshop.update(workshop_params.except(:speaker_id))
        format.json { render :show, status: :ok }
      else
        format.json { render json: @workshop.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /workshops/1
  def destroy
    @workshop.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  def brief
    @workshops = Workshop.where(is_approved: true)
  end

  def signed
    @workshops = @user.subscribed_workshops
  end

  def approval_requests
    @approval_requests = ApprovalRequest.where.not(status: :approved)
  end

  def subscribe
    is_not_already_subscribed = @workshop.subscribers.include? (@user)
    is_not_filled = @workshop.subscribers.count < @workshop.limit_room
    is_not_the_speaker = @workshop.speaker != @user
    
    if (is_not_already_subscribed && is_not_the_speaker && is_not_filled)
      @workshop.subscribers << @user
      respond_to do |format|
        format.json { head :ok }
      end
    else
      respond_to do |format|
        format.json { head :forbidden }
      end
    end
  end

  def unsubscribe
    @workshop.subscribers.delete(@user)
    respond_to do |format|
      format.json { head :ok }
    end
  end

  def speaker_workshops
    @workshops = @user.workshops
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_workshop
      @workshop = Workshop.find(params[:id])
    end

    def set_user
      @user = User.find_by(email: request.headers["uid"]) if @user.nil?
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def workshop_params
      params.require(:workshop).permit(
        :name, 
        :description, 
        :banner_url, 
        :documentation_url, 
        :begin_date, 
        :end_date, 
        :limit_room, 
        :speaker_id)
    end
end
