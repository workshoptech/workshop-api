class ApplicationController < ActionController::API
  include DeviseTokenAuth::Concerns::SetUserByToken
  include ActionController::MimeResponds
  before_action :configure_permitted_parameters, if: :devise_controller?

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [
      :name,
      :lastname,
      :registration,
      :role,
      :role_password,
      :company
    ])
    devise_parameter_sanitizer.permit(:account_update, keys: [
      :name,
      :lastname,
      :photo_url,
      :email,
      :achievements_score,
      :role,
      :phone,
      :facebook_url,
      :instagram_url,
      :linkedin_url,
      :academic_level,
      :trello_token,
      :trello_id,
      :trello_username,
      :company
    ])
  end

  def set_user
    @user = User.find_by(email: request.headers["uid"])
  end

  def account_is_admin
    current_api_user.super_admin? or current_api_user.admin?
  end

  def account_is_intern
    current_api_user.intern?
  end
end
