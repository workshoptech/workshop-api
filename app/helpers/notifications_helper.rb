module NotificationsHelper
  def create_notification(destinatary, notificable)
    notification = Notification.new(
      destinatary: destinatary,
      notificable: notificable
    )
    send_firebase_notification(notification) if notification.save!
  end

  def create_workshop_notification(destinatary, notificable, workshop)
    notification = Notification.new(
      destinatary: destinatary,
      notificable: notificable,
      workshop: workshop
    )
    send_firebase_notification(notification) if notification.save!
  end

  def send_firebase_notification(notification)
    # TODO: Implement this method
  end
end
