module TasksHelper
    require 'net/http'
    require 'uri'

    # Trello credentials
    @on_review_list_id = '5ca248fcd19850741598d94a'
    @done_list_id = '5ca23d5f44c8a56d2903f0a8'

    def refresh_trello_tasks
      process_board ENV["TRELLO_YELLOWME_BOARD_ID"]
      process_board ENV["TRELLO_WALOOK_BOARD_ID"]
      process_board ENV["TRELLO_COATILABS_BOARD_ID"]
      process_board ENV["TRELLO_KSQUARE_BOARD_ID"]
      process_board ENV["TRELLO_SOLDAI_BOARD_ID"]
      process_board ENV["TRELLO_GLEO_BOARD_ID"]
      process_board ENV["TRELLO_CUBICODE_BOARD_ID"]
      process_board ENV["TRELLO_MIDSOFTWARE_BOARD_ID"]
    end

    private

    def process_board boardId
      set_trello_lists_id boardId
      match_trello_users_of_board boardId
      get_trello_cards_of_board boardId
    end

    def set_trello_lists_id boardId
      get_lists_path = "https://api.trello.com/1/boards/#{boardId}/lists?key=#{ENV["TRELLO_API_KEY"]}&token=#{ENV["TRELLO_USER_TOKEN"]}"
      trello_lists = make_request(get_lists_path)
      
      for list in trello_lists do
        case list['name']
        when 'ON REVIEW'
          @on_review_list_id = list['id']
        when 'DONE'
          @done_list_id = list['id']
        end
      end
    end

    def match_trello_users_of_board boardId
      get_members_path = "https://api.trello.com/1/boards/#{boardId}/members?key=#{ENV["TRELLO_API_KEY"]}&token=#{ENV["TRELLO_USER_TOKEN"]}"
      trello_members = make_request(get_members_path)
      for member in trello_members do
        user = User.find_by(trello_username: member['username'])
        if user
          user.trello_id = member['id']
          user.save
        end
      end
    end

    def get_trello_cards_of_board boardId
      get_trello_cards_path = "https://api.trello.com/1/boards/#{boardId}/cards?customFieldItems=true&key=#{ENV["TRELLO_API_KEY"]}&token=#{ENV["TRELLO_USER_TOKEN"]}"
      trello_cards = make_request(get_trello_cards_path)
      for card in trello_cards do
        usersOfCards = User.where(trello_id: card['idMembers'])
        task = Task.find_or_create_by(trello_card_id: card['id'])
        task.title = card['name']
        task.description = card['desc']

        if card['customFieldItems'].any?
          worked_time = card['customFieldItems'][0]['value']['number']
          task.worked_time = worked_time
        end

        for user in usersOfCards
          task.users << user unless task.users.include? (user)  
        end

        case card['idList']
        when @on_review_list_id
          task.status = :on_review
        when @done_list_id
          task.status = :done
        else
          task.status = :todo
        end

        task.save
      end
    end
    
    def make_request url
      url = URI.parse(url)
      http = Net::HTTP.new(url.host, 443)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      begin
        request = Net::HTTP::Get.new(url.request_uri)
        request["Content-Type"] = "application/json"
        Rails.logger.warn url
        response = http.request(request)
        body = JSON(response.body)
        return body
      rescue => e
        puts '============================= Something was wrong ============================='
        puts e
        raise e
      end
    end
end
