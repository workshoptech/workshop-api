class Achievement < ApplicationRecord
 has_one :icon, class_name: "image"

 belongs_to :workshop, class_name: "workshop", foreign_key: "workshop_id"

end
