class ApprovalRequest < ApplicationRecord
  belongs_to :workshop, class_name: :Workshop, foreign_key: "workshop_id"
  belongs_to :speaker, class_name: :User, foreign_key: "user_id"
  has_many :notifications, :as => :notificable

  enum status: {
    approved: 'approved',
    rejected: 'rejected',
    pending: 'pending'
  }
end
