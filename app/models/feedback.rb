class Feedback < ApplicationRecord
 belongs_to :workshop, class_name: "workshop", foreign_key: "workshop_id"
 belongs_to :feedback_by, class_name: "user", foreign_key: "user_id"
 
end
