class Inscription < ApplicationRecord
belongs_to :user, class_name: "user", foreign_key: "user_id"
belongs_to :workshop, class_name: "workshop", foreign_key: "workshop_id"

end
