class Message < ApplicationRecord
  belongs_to :author, class_name: "User", :foreign_key => "author_id"

  enum destinatary_type: {
    general: 'general',
    workshop: 'workshop',
    user: 'user'
  }

  def created_at_timestamp
    self.created_at.to_i
  end

  def updated_at_timestamp
    self.updated_at.to_i
  end
  
end
