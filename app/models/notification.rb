class Notification < ApplicationRecord
 belongs_to :destinatary, class_name: "User", foreign_key: "user_id"
 belongs_to :workshop, class_name: "Workshop", foreign_key: "workshop_id", optional: true
 belongs_to :notificable, polymorphic: true, autosave: true

  def created_at_timestamp
    self.created_at.to_i
  end

  def updated_at_timestamp
    self.updated_at.to_i
  end

end
