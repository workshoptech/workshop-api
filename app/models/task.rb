class Task < ApplicationRecord
  has_and_belongs_to_many :users
  belongs_to :signed_by, class_name: "user", foreign_key: "signed_by_id", required: false


  enum status: {
    todo: 'todo',
    doing: 'doing',
    on_review: 'on_review',
    done: 'done'
  }

  def created_at_timestamp
    self.created_at.to_i
  end

  def updated_at_timestamp
    self.updated_at.to_i
  end

end
