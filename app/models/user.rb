class User < ApplicationRecord
    # Include default devise modules. O thers available are:
    # :confirmable, :lockable, :timeoutable and :omniauthable
    devise :database_authenticatable, :registerable,
        :recoverable, :rememberable, :trackable, :validatable
    include DeviseTokenAuth::Concerns::User
    has_and_belongs_to_many :tasks
    has_and_belongs_to_many :subscribed_workshops, class_name: :Workshop, join_table: :workshops_users
    has_many :notifications, foreign_key: :user_id
    has_many :workshops, foreign_key: :speaker_id
    validate :has_correct_role_password, :on => :create

    enum role: {
        super_admin: 'super_admin',
        admin: 'admin',
        intern: 'intern',
        student: 'student'
    }

    enum academic_level: {
        bachelor: 'bachelor',
        master: 'master',
        doctor: 'doctor',
        other: 'other'
    }

    private
        def has_correct_role_password
            role_key = ''
            case self.role
            when User.roles[:admin]
                role_key = ENV["ADMIN_KEY"]
            when User.roles[:super_admin]
                role_key = ENV["SUPER_ADMIN_KEY"]
            when User.roles[:intern]
                role_key = ENV["INTERN_KEY"]
            else
                role_key = self.role_password
            end

            unless self.role_password == role_key
                errors.add(:user, 'wrong role password')
            end
        end
end
