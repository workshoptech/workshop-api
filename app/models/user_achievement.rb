class UserAchievement < ApplicationRecord
 belongs_to :user, class_name: "user", foreign_key: "user_id"
 belongs_to :achievement, class_name: "achievement", foreign_key: "achievement_id"

end
