class Workshop < ApplicationRecord
  belongs_to :speaker, class_name: :User
  has_and_belongs_to_many :subscribers, class_name: :User, join_table: :workshops_users
  has_many :approval_requests

  validates :name, :begin_date, :speaker_id, :presence => true
  validates :limit_room, numericality: { greater_than: 0 }

  def status
    if self.end_date.nil?
      return self.begin_date >= Date.today ? "coming" : "passed"
    else
      return self.begin_date >= Date.today ? 
        "coming" : self.end_date >= Date.today ? "going" : "passed"
    end
  end
end
