json.extract! approval_request, :id, :comment, :status, :created_at, :updated_at

json.speaker do
  json.partial! "api/users/user_short", user: approval_request.speaker
end

json.workshop do
  json.extract! approval_request.workshop, :id, :name, :banner_url, :description, :begin_date, :end_date
end
