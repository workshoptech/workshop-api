json.extract! message, :id, :body, :destinatary_type, :created_at_timestamp, :updated_at_timestamp
json.author do
  json.name message.author.name
  json.lastname message.author.lastname
  json.id message.author.id
  json.photo_url message.author.photo_url
end
