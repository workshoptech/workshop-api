json.workshop @workshops do |workshop|
  json.name workshop.name
  json.id workshop.id
end

json.users @users do |user|
  json.name user.name
  json.lastname user.lastname
  json.id user.id
end