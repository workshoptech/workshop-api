json.extract! notification, :id, :notificable_type, :created_at_timestamp, :updated_at_timestamp

json.destinatary do
  json.extract! notification.destinatary, :id, :name, :lastname, :photo_url
end

case notification.notificable_type
when "ApprovalRequest"
  approval_request = notification.notificable
  
  json.approval_request do
    json.extract! approval_request, :id, :status, :comment
    json.speaker do
      json.extract! approval_request.speaker, :id, :name, :lastname
    end
    json.workshop do
      json.extract! approval_request.workshop, :id, :name, :banner_url
    end
  end
  
  json.author_photo_url approval_request.speaker.photo_url
when "Message"
  message = notification.notificable

  json.message do
    json.id message.id
    json.body message.body
    json.destinatary_type message.destinatary_type
    
    json.author do
      json.extract! message.author, :id, :name, :lastname
    end
  end

  json.author_photo_url message.author.photo_url
end