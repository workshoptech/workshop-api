json.extract! task, :id, :title, :description, :worked_time, :status, :signed_by_id, :created_at_timestamp, :updated_at_timestamp
json.users task.users do |user|
    json.id user.id
end