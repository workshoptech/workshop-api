json.array! @tasks do |task|
    json.user_id task[0]
    json.user_name task[1]
    json.user_lastname task[2]
    json.numberOfTasksOnReview task[3]
    json.user_photo_url task[4]
    json.total_worked_time task[5]
end
