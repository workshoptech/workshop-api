json.array! @users do |user|
    json.id user.id
    json.photoUrl user.photo_url
    json.name user.name
    json.lastname user.lastname
    json.role user.role
    json.company user.company
    json.score user.achievements_score
    json.trello_username user.trello_username
    json.trello_id user.trello_id
    json.trello_token user.trello_token
end
