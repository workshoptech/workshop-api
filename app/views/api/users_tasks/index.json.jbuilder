json.name @user_name
json.lastname @user_lastname
json.total_worked_time @total_worked_time
json.tasks do 
    json.partial! 'api/tasks/task', collection: @tasks, as: :task
end
