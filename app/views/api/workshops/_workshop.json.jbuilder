json.extract! workshop, :name, :id, :description, :banner_url, :documentation_url, :begin_date, :end_date, :limit_room, :status, :created_at, :updated_at

if @user
  json.is_subscribed workshop.subscribers.exists?(@user.id)
end

json.speaker do
  json.partial! "api/users/user", user: workshop.speaker
end

json.subscribers workshop.subscribers do |subscriber|
  json.partial! "api/users/user_short", user: subscriber
end
