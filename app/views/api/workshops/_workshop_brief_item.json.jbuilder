json.extract! workshop, :id, :name, :banner_url, :begin_date, :end_date, :status
json.subscribers workshop.subscribers do |subscriber|
  json.id subscriber.id
  json.name subscriber.name
  json.lastname subscriber.lastname
  json.photo_url subscriber.photo_url
end