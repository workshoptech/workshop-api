json.extract! workshop, :name, :id, :name, :banner_url, :begin_date, :end_date, :status
json.speaker_id workshop.speaker.id
json.speaker_name workshop.speaker.name
json.subscribers workshop.subscribers do |subscriber|
  json.id subscriber.id
  json.name subscriber.name
  json.lastname subscriber.lastname
  json.photo_url subscriber.photo_url
end

if @user
  json.is_subscribed workshop.subscribers.exists?(@user.id) 
end