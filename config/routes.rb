Rails.application.routes.draw do
  namespace :api, :defaults  => { format: :json } do
    mount_devise_token_auth_for 'User', at: 'auth'
    resources :achievements
    resources :feedbacks
    resources :images
    resources :inscriptions
    resources :messages do
      collection do
        get 'destinataries'
      end
    end
    resources :notifications, only: [:index] do
      collection do
        get 'brief'
      end
    end
    resources :users, only: [:index, :show, :update, :destroy] do
      resources :tasks, only: [:index, :show], controller: :users_tasks
      member { get 'workshops', to: 'workshops#speaker_workshops' }
      member { get 'signed_workshops', to: 'workshops#signed' }
      member { get 'notifications', to: 'notifications#by_user' }
    end
    resources :user_achievements
    resources :tasks
    resources :testimonies
    resources :workshops do
      resources :approval_requests, only: [:create, :index, :show, :update,:destroy]
      member do
        post 'subscribe'
        post 'unsubscribe'
      end
      collection do
        get 'brief'
        get 'signed'
        get 'approval_requests'
        get 'mine', to: 'workshops#speaker_workshops'
      end
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
