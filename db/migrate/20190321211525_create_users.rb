class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :lastname
      t.string :photo_url
      t.string :email
      t.integer :achievements_score, default: 0
      t.integer :phone
      t.string :facebook_url
      t.string :instagram_url
      t.string :linkedin_url
      t.string :role, default: :normal
      t.string :academic_level

      t.string :trello_token
      t.timestamps
    end
  end
end
