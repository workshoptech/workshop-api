class CreateWorkshops < ActiveRecord::Migration[5.2]
  def change
    create_table :workshops do |t|
      t.string :name
      t.text :description
      t.string :banner_url
      t.string :documentation_url
      t.timestamp :begin_date
      t.timestamp :end_date
      t.integer :limit_room
      t.timestamps
    end
  end
end
