class CreateInscriptions < ActiveRecord::Migration[5.2]
  def change
    create_table :inscriptions do |t|
      t.belongs_to :user
      t.belongs_to :workshop
      t.timestamps
    end
  end
end
