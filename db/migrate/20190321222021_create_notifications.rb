class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications do |t|
      t.boolean :seen
      t.text :message
      t.belongs_to :user
      t.belongs_to :workshop
      t.timestamps
    end
  end
end
