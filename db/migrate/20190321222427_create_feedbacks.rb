class CreateFeedbacks < ActiveRecord::Migration[5.2]
  def change
    create_table :feedbacks do |t|
      t.integer :rating, default: 0
      t.text :comment
      t.belongs_to :user
      t.belongs_to :workshop
      
      t.timestamps
    end
  end
end
