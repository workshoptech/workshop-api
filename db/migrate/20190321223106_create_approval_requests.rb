class CreateApprovalRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :approval_requests do |t|
      t.text :comment
      t.string :status, default: 'pending'
      t.belongs_to :workshop
      t.belongs_to :user

      t.timestamps
    end
  end
end
