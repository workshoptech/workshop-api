class CreateAchievements < ActiveRecord::Migration[5.2]
  def change
    create_table :achievements do |t|
      t.string :icon_url
      t.string :name
      t.text :description
      t.integer :score
      t.belongs_to :workshop
      
      t.timestamps
    end
  end
end
