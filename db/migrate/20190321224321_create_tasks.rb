class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.string :title
      t.text :description
      t.integer :worked_time
      t.string :status
      t.references :done_by
      t.references :signed_by
      t.timestamps
    end
    add_foreign_key :tasks, :users, column: :done_by_id, primary_key: :id
    add_foreign_key :tasks, :users, column: :signed_by_id, primary_key: :id
  end
end
