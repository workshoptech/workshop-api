class AddTrelloIdToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :trello_id, :string
    add_column :users, :trello_username, :string
  end
end
