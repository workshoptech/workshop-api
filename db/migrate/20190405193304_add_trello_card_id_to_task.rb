class AddTrelloCardIdToTask < ActiveRecord::Migration[5.2]
  def change
    add_column :tasks, :trello_card_id, :string
  end
end
