class AddRelationsToWorkshops < ActiveRecord::Migration[5.2]
  def change
    add_index :workshops, :name, unique: true
    add_reference :workshops, :speaker, references: :users, index: true
    add_foreign_key :workshops, :users, column: :speaker_id
    change_column :workshops, :begin_date, :datetime

    create_table :workshops_users, id: false do |t|
      t.belongs_to :user, index: true
      t.belongs_to :workshop, index: true
    end
  end
end
