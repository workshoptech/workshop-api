class AddRolePasswordToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :role_password, :string
  end
end
