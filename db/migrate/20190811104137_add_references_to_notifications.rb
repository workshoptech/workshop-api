class AddReferencesToNotifications < ActiveRecord::Migration[5.2]
  def change
    change_table :notifications do |t|
      t.references :notificable, polymorphic: true, index: true
    end
    create_table :notifications_users, id: false do |t|
      t.belongs_to :user, index: true
      t.belongs_to :notification, index: true
    end
  end
end
