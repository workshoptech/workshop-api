class AddDestinataryTypeToMessages < ActiveRecord::Migration[5.2]
  def change
    add_column :messages, :destinatary_type, :string
  end
end
