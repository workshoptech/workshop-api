class AddApprovalStatusToWorkshops < ActiveRecord::Migration[5.2]
  def change
    add_column :workshops, :is_approved, :boolean, :default => false
  end
end
