require 'test_helper'

class ApprovalRequestsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @approval_request = approval_requests(:one)
  end

  test "should get index" do
    get approval_requests_url
    assert_response :success
  end

  test "should get new" do
    get new_approval_request_url
    assert_response :success
  end

  test "should create approval_request" do
    assert_difference('ApprovalRequest.count') do
      post approval_requests_url, params: { approval_request: {  } }
    end

    assert_redirected_to approval_request_url(ApprovalRequest.last)
  end

  test "should show approval_request" do
    get approval_request_url(@approval_request)
    assert_response :success
  end

  test "should get edit" do
    get edit_approval_request_url(@approval_request)
    assert_response :success
  end

  test "should update approval_request" do
    patch approval_request_url(@approval_request), params: { approval_request: {  } }
    assert_redirected_to approval_request_url(@approval_request)
  end

  test "should destroy approval_request" do
    assert_difference('ApprovalRequest.count', -1) do
      delete approval_request_url(@approval_request)
    end

    assert_redirected_to approval_requests_url
  end
end
