require "application_system_test_case"

class ApprovalRequestsTest < ApplicationSystemTestCase
  setup do
    @approval_request = approval_requests(:one)
  end

  test "visiting the index" do
    visit approval_requests_url
    assert_selector "h1", text: "Approval Requests"
  end

  test "creating a Approval request" do
    visit approval_requests_url
    click_on "New Approval Request"

    click_on "Create Approval request"

    assert_text "Approval request was successfully created"
    click_on "Back"
  end

  test "updating a Approval request" do
    visit approval_requests_url
    click_on "Edit", match: :first

    click_on "Update Approval request"

    assert_text "Approval request was successfully updated"
    click_on "Back"
  end

  test "destroying a Approval request" do
    visit approval_requests_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Approval request was successfully destroyed"
  end
end
